#include "Download.hpp"

#include <iostream>
#include <charconv>
#include "util.hpp"
#include <openssl/sha.h>
#include <filesystem>

using tcp = boost::asio::ip::tcp;
namespace http = boost::beast::http;

Download::Download(
    boost::asio::io_context & ioContext,
    std::string_view clientID,
    const char * torrentFileName,
    std::uint_least16_t listenPort
) :
    mIoContext{ ioContext },
    mAnnounce{
        tcp::resolver{ ioContext },
        tcp::socket{ ioContext },
        {}
    },
    mListenPort{ listenPort }
{
    if (mListenPort > 65535) {
        throw std::runtime_error("Invalid port.");
    }
    if (clientID.size() != 20)
        return;
    
    std::copy(clientID.begin(), clientID.end(), mClientID.begin());

    mTorrentFileData = util::loadFile(torrentFileName);
    const auto torrentFileIndex = bencode::decode(mTorrentFileData.data(), mTorrentFileData.data() + mTorrentFileData.size());
    mTorrentFileView = bencode::indexNodeToViewNode(mTorrentFileData.data(), torrentFileIndex);
    if (mTorrentFileView.size() < 1) return;

    auto checkFindKeyResult = [this] (
        const std::ptrdiff_t index,
        const bencode::type desiredType
    ) {
        if (index < 0 || index >= mTorrentFileView.size())
            return false;
        if (mTorrentFileView[index].node_type != desiredType)
            return false;
        return true;
    };

    const std::ptrdiff_t rootDictIndex = 0;
    if (!checkFindKeyResult(rootDictIndex, bencode::type::DICTIONARY)) return;
    mAnnounceIndex = bencode::findKey(mTorrentFileView, rootDictIndex, "announce");
    if (!checkFindKeyResult(mAnnounceIndex, bencode::type::STRING)) return;
    mInfoIndex = bencode::findKey(mTorrentFileView, rootDictIndex, "info");
    if (!checkFindKeyResult(mInfoIndex, bencode::type::DICTIONARY)) return;
    mPieceLengthIndex = bencode::findKey(mTorrentFileView, mInfoIndex, "piece length");
    if (!checkFindKeyResult(mPieceLengthIndex, bencode::type::INTEGER)) return;
    mPieceHashIndex = bencode::findKey(mTorrentFileView, mInfoIndex, "pieces");
    if (!checkFindKeyResult(mPieceHashIndex, bencode::type::STRING)) return;
    mLengthIndex = bencode::findKey(mTorrentFileView, mInfoIndex, "length");
    if (!checkFindKeyResult(mLengthIndex, bencode::type::INTEGER)) return;
    mNameIndex = bencode::findKey(mTorrentFileView, mInfoIndex, "name");
    if (!checkFindKeyResult(mNameIndex, bencode::type::STRING)) return;

    const auto pieceLengthConvResult = std::from_chars(
        mTorrentFileView[mPieceLengthIndex].data.begin(),
        mTorrentFileView[mPieceLengthIndex].data.end(),
        mPieceLength, 10
    );
    if (pieceLengthConvResult.ec != std::errc())
        return;
    if (pieceLengthConvResult.ptr != mTorrentFileView[mPieceLengthIndex].data.end())
        return;
    if (mPieceLength < 1)
        return;

    const auto lengthConvResult = std::from_chars(
        mTorrentFileView[mLengthIndex].data.begin(),
        mTorrentFileView[mLengthIndex].data.end(),
        mLength, 10
    );
    if (lengthConvResult.ec != std::errc())
        return;
    if (lengthConvResult.ptr != mTorrentFileView[mLengthIndex].data.end())
        return;
    if (mLength < 1)
        return;
    
    mPieceCount = mLength / mPieceLength;
    mLastPieceLength = mLength % mPieceLength;
    if (mLastPieceLength != 0)
        mPieceCount += 1;
    assert(mPieceCount > 0);
    mLastPiecePosition = mPieceCount - 1;

    // verify piece count
    const auto piecesHashSize = mTorrentFileView[mPieceHashIndex].data.size();
    if (piecesHashSize % SHA_DIGEST_LENGTH != 0)
        return;
    if (mPieceCount != piecesHashSize / SHA_DIGEST_LENGTH)
        return;

    mPieceBitField = util::bit_vector{ mPieceCount };

    // open, create file
    std::filesystem::path filePath{ mTorrentFileView[mNameIndex].data };
    mFile.open(
        filePath,
        std::fstream::binary | std::fstream::in | std::fstream::out
    );

    if (!mFile) {
        mFile.open(
            filePath,
            std::fstream::binary | std::fstream::in | std::fstream::out | std::fstream::trunc
        );
        std::cout << filePath.c_str() << " File does not exists or can not be opened, start download from beginning." << std::endl;
        setupNewFile();
    } else {
        setupExistingFile();
        std::cout << filePath.c_str() << " File already exists, continue downloading or seeding." << std::endl;
    }

    const auto infoBruttoView = bencode::bruttoView(mTorrentFileView, mInfoIndex);

    static_assert(SHA_DIGEST_LENGTH == decltype(mInfoHash)().size());
    SHA1(
        reinterpret_cast<const unsigned char *>(infoBruttoView.data()),
        infoBruttoView.size(),
        reinterpret_cast<unsigned char *>(mInfoHash.data())
    );

    util::toPercentEncoding({ mInfoHash.data(), mInfoHash.size() }, mInfoHashPercent.data(), mInfoHashPercent.size());

    std::cout << "announce:            " << mTorrentFileView[mAnnounceIndex].data << std::endl;
    std::cout << "piece length:        " << mTorrentFileView[mPieceLengthIndex].data << std::endl;
    std::cout << "pieces (byte count): " << mTorrentFileView[mPieceHashIndex].data.size() << std::endl;
    std::cout << "length:              " << mTorrentFileView[mLengthIndex].data << std::endl;
    std::cout << "name:                " << mTorrentFileView[mNameIndex].data << std::endl;

    scheduleAnnounce();
}

gsl::span<const char> Download::getPieceHash(std::ptrdiff_t pieceIndex) {
    if (pieceIndex < 0 || pieceIndex >= mPieceCount)
        throw std::runtime_error("Piece hash out of bounds access.");
    return {
        mTorrentFileView[mPieceHashIndex].data.data() + pieceIndex * SHA_DIGEST_LENGTH,
        SHA_DIGEST_LENGTH
    };
}

void Download::writeFile(const char * data, std::ptrdiff_t byteCount, std::ptrdiff_t byteOffset) {
    // TODO: check for success
    mFile.seekp(byteOffset);
    mFile.write(data, byteCount);
}

void Download::readFile(char * data, std::ptrdiff_t byteCount, std::ptrdiff_t byteOffset) {
    // TODO: check for success
    mFile.seekg(byteOffset);
    mFile.read(data, byteCount);
}

void Download::setupNewFile() {
    std::cout << "Setting up new file." << std::endl;
    if (mPieceLength < 1)
        throw std::runtime_error("Handling files of size 0 is not implemented.");
    std::vector<char> dummyPiece(mPieceLength, 0);
    std::vector<char> dummyPieceAlt(mPieceLength, 0);
    std::vector<char> dummyPieceLast(mLastPieceLength, 0);
    std::vector<char> dummyPieceLastAlt(mLastPieceLength, 0);
    std::array<char, SHA_DIGEST_LENGTH> dummyPieceHash;
    std::array<char, SHA_DIGEST_LENGTH> dummyPieceHashAlt;
    std::array<char, SHA_DIGEST_LENGTH> dummyPieceLastHash;
    std::array<char, SHA_DIGEST_LENGTH> dummyPieceLastHashAlt;

    util::charSHA1(dummyPiece.data(), dummyPiece.size(), dummyPieceHash.data());

    while (util::equalSHA1(dummyPieceHash, dummyPieceHashAlt)) {
        assert(dummyPieceAlt.size() > 0);
        if (dummyPieceAlt[0] == std::numeric_limits<char>::max())
            throw std::runtime_error("Unable to find two pieces with different SHA1s, lol, a better method can be implemented.");
        ++dummyPieceAlt[0];
        util::charSHA1(dummyPieceAlt.data(), dummyPieceAlt.size(), dummyPieceHashAlt.data());
    }

    util::charSHA1(dummyPieceLast.data(), dummyPieceLast.size(), dummyPieceLastHash.data());

    while (util::equalSHA1(dummyPieceLastHash, dummyPieceLastHashAlt)) {
        assert(dummyPieceLastAlt.size() > 0);
        if (dummyPieceLastAlt[0] == std::numeric_limits<char>::max())
            throw std::runtime_error("Unable to find two pieces with different SHA1s, lol, a better method can be implemented.");
        ++dummyPieceLastAlt[0];
        util::charSHA1(dummyPieceLastAlt.data(), dummyPieceLastAlt.size(), dummyPieceLastHashAlt.data());
    }

    assert(mPieceBitField.bitCount() == mPieceCount);
    for (std::ptrdiff_t i = 0; i < mPieceCount; ++i) {
        mPieceBitField.clear(i);

        const auto pieceHash = getPieceHash(i);
        if (i == mLastPiecePosition) {
            if (util::equalSHA1(pieceHash, dummyPieceLastHash)) {
                writeFile(dummyPieceLastAlt.data(), dummyPieceLastAlt.size(), mPieceLength * i);
            } else {
                writeFile(dummyPieceLast.data(), dummyPieceLast.size(), mPieceLength * i);
            }
        } else {
            if (util::equalSHA1(pieceHash, dummyPieceHash)) {
                writeFile(dummyPieceAlt.data(), dummyPieceAlt.size(), mPieceLength * i);
            } else {
                writeFile(dummyPiece.data(), dummyPiece.size(), mPieceLength * i);
            }
        }
    }
}

void Download::setupExistingFile() {
    std::cout << "Setting up existing file." << std::endl;
    assert(mPieceBitField.bitCount() == mPieceCount);
    std::vector<char> piece;
    piece.resize(mPieceLength);
    bool fileDone = true;
    for (std::ptrdiff_t i = 0; i < mPieceCount; ++i) {
        const auto pieceHash = getPieceHash(i);
        if (i == mLastPiecePosition) {
            piece.resize(mLastPieceLength);
            readFile(piece.data(), mLastPieceLength, i * mPieceLength);
        } else {
            assert(piece.size() == mPieceLength);
            readFile(piece.data(), mPieceLength, i * mPieceLength);
        }
        std::array<char, SHA_DIGEST_LENGTH> hash;
        util::charSHA1(piece.data(), piece.size(), hash.data());
        if (util::equalSHA1(hash, pieceHash)) {
            mPieceBitField.set(i);
        } else {
            fileDone = false;
            mPieceBitField.clear(i);
        }
    }
    if (fileDone) {
        std::cout << "Opened file is finished, seeding I guess." << std::endl;
    }
}


void Download::scheduleAnnounce() {
    static constexpr const char * USER_AGENT_NAME = "testprogram";

    // TODO: compute these parameters
    const std::ptrdiff_t numwant = 3;
    const char * event = "started";
    const bool include_event = true;

    const auto uri_info = util::parseURI(mTorrentFileView[mAnnounceIndex].data);
    std::ostringstream target;
    target << uri_info.target;
    target << "?info_hash=";
    target.write(mInfoHashPercent.data(), mInfoHashPercent.size());
    target << "&peer_id=";
    target.write(mClientID.data(), mClientID.size());
    target << "&port=";
    target << mListenPort;
    target << "&uploaded=";
    target << mUploaded;
    target << "&downloaded=";
    target << mDownloaded;
    target << "&left=";
    target << mLeft;
    target << "&compact=0";
    target << "&numwant=";
    target << numwant;
    if (include_event) {
        target << "&event=";
        target << event;
    }
    target << "&supportcrypto=0";

    const std::string tt = target.str();
    //std::cout << tt << std::endl;

    mAnnounce.request.version(11);
    mAnnounce.request.method(http::verb::get);
    mAnnounce.request.target(tt);
    mAnnounce.request.set(http::field::host, uri_info.host);
    mAnnounce.request.set(http::field::user_agent, USER_AGENT_NAME);

    mAnnounce.resolver.async_resolve(
        uri_info.host,
        uri_info.port,
        [this] (
            const boost::system::error_code & error,
            tcp::resolver::results_type results
        ) { onResolveAnnounce(error, results); }
    );
}

void Download::onResolveAnnounce(
    const boost::system::error_code & error,
    tcp::resolver::results_type results
) {
    if (error) return;
    boost::asio::async_connect(
        mAnnounce.socket,
        results.begin(), results.end(),
        [this] (
            const boost::system::error_code & error,
            tcp::resolver::results_type::const_iterator i
        ) { onConnectAnnounce(error, i); }
    );
}

void Download::onConnectAnnounce(
    const boost::system::error_code & error,
    tcp::resolver::results_type::const_iterator i
) {
    if (error) return;
    http::async_write(
        mAnnounce.socket,
        mAnnounce.request,
        [this] (
            const boost::system::error_code & error,
            std::size_t bytesTransferred
        ) { onWriteAnnounce(error, bytesTransferred); }
    );
}

void Download::onWriteAnnounce(
    const boost::system::error_code & error,
    std::size_t bytesTransferred
) {
    if (error) return;
    http::async_read(
        mAnnounce.socket,
        mAnnounce.buffer,
        mAnnounce.response,
        [this] (const boost::system::error_code & error, std::size_t bytesTransferred) {
            onReadAnnounce(error, bytesTransferred);
        }
    );
}

void Download::onReadAnnounce(
    const boost::system::error_code & error,
    std::size_t bytesTransferred
) {
    if (error) return;
    boost::system::error_code ec;
    mAnnounce.socket.shutdown(tcp::socket::shutdown_both, ec);
    if (ec && ec != boost::system::errc::not_connected)
        return;
    
    mAnnounce.responseBody.resize(0);
    std::copy(mAnnounce.response.body().begin(), mAnnounce.response.body().end(), std::back_inserter(mAnnounce.responseBody));
    const auto tmp = bencode::decode(mAnnounce.responseBody.data(), mAnnounce.responseBody.data() + mAnnounce.responseBody.size());
    mAnnounce.responseView = bencode::indexNodeToViewNode(mAnnounce.responseBody.data(), tmp);
    std::cout.write(mAnnounce.responseBody.data(), mAnnounce.responseBody.size());
    std::cout << std::endl;
    auto checkFindKeyResult = [this] (
        const std::ptrdiff_t index,
        const bencode::type desiredType
    ) {
        if (index < 0 || index >= mAnnounce.responseView.size())
            return false;
        if (mAnnounce.responseView[index].node_type != desiredType)
            return false;
        return true;
    };

    const std::ptrdiff_t rootDictIndex = 0;
    if (!checkFindKeyResult(rootDictIndex, bencode::type::DICTIONARY)) return;
    const auto peersIndex = bencode::findKey(mAnnounce.responseView, rootDictIndex, "peers");
    if (!checkFindKeyResult(peersIndex, bencode::type::LIST)) return;

    auto peerIndex = mAnnounce.responseView[peersIndex].child_index;
    while (peerIndex >= 0 && peerIndex < mAnnounce.responseView.size()) {
        if (!checkFindKeyResult(peerIndex, bencode::type::DICTIONARY)) return;
        const auto ipIndex = bencode::findKey(mAnnounce.responseView, peerIndex, "ip");
        if (!checkFindKeyResult(ipIndex, bencode::type::STRING)) return;
        const auto peerIdIndex = bencode::findKey(mAnnounce.responseView, peerIndex, "peer id");
        if (!checkFindKeyResult(peerIdIndex, bencode::type::STRING)) return;
        const auto portIndex = bencode::findKey(mAnnounce.responseView, peerIndex, "port");
        if (!checkFindKeyResult(portIndex, bencode::type::INTEGER)) return;
        PeerKey peerKey;
        auto ipView = mAnnounce.responseView[ipIndex].data;
        if (ipView.size() > 2) {
            if (*ipView.begin() == '[' && *(ipView.end() - 1) == ']') {
                ipView = ipView.substr(1, ipView.size() - 2);
            }
        }
        bool ipGood = true;
        try {
            peerKey.mAddress = boost::asio::ip::make_address(ipView);
        } catch (...) {
            ipGood = false;
        }
        if (!ipGood) {
            peerIndex = mAnnounce.responseView[peerIndex].next_index;
            continue;
        }
        const auto portConvResult = std::from_chars(
            mAnnounce.responseView[portIndex].data.data(),
            mAnnounce.responseView[portIndex].data.data() + mAnnounce.responseView[portIndex].data.size(),
            peerKey.mPort
        );
        if (portConvResult.ec != std::errc())
            return;
        if (peerKey.mPort < 0 || peerKey.mPort > 65535)
            return;
        if (mAnnounce.responseView[peerIdIndex].data.size() != peerKey.mPeerID.size())
            return;
        std::copy(mAnnounce.responseView[peerIdIndex].data.begin(), mAnnounce.responseView[peerIdIndex].data.end(), peerKey.mPeerID.data());

        auto peerSearch = mPeersFromTracker.find(peerKey);
        auto peerSearchActive = mPeersFromTracker.find(peerKey);
        if (peerSearch == mPeersFromTracker.end() && peerSearchActive == mActivePeers.end()) {
            auto peerNode = mPeersFromTracker.emplace(
                std::piecewise_construct,
                std::forward_as_tuple(peerKey),
                std::forward_as_tuple(mIoContext, mPieceBitField.bitCount(), MAX_MESSAGE_LENGTH, MAX_MESSAGE_LENGTH)
            );
            if (peerNode.second != true) {
                // TODO: what? that should not happen
                throw std::runtime_error("Broken unordered_map ?");
            }

            // pass reference because the iterator can get invalidated (unlike the reference)
            schedulePeerConnect(*peerNode.first);
        }

        peerIndex = mAnnounce.responseView[peerIndex].next_index;
    }

    // TODO: re-schedule tracker announce
    // TODO: send stopped or completed or whatever the right message is in the rescheduled message
}

void Download::schedulePeerConnect(PeerMapNode & peer) {

    tcp::endpoint endpoint{ peer.first.mAddress, peer.first.mPort };
    peer.second.mSocket.async_connect(
        endpoint,
        [this, &peer] (boost::system::error_code ec) {
            onPeerConnect(ec, peer);
        }
    );
}

void Download::onPeerConnect(boost::system::error_code ec, PeerMapNode & peer) {
    if (ec) {
        mPeersFromTracker.erase(peer.first);
        std::cout << "Failed to connect to a peer." << std::endl;
        // TODO: handle that
        return;
    }

    // send handshake
    auto & handshake = peer.second.mTxBuffer;
    handshake.clear();
    handshake.reserve(1 + 19 + 8 + 20 + 20);
    handshake.push_back(19);
    std::copy(PROTOCOL_NAME, PROTOCOL_NAME + 19, std::back_inserter(handshake));
    for (int i = 0; i < 8; ++i) handshake.push_back(0);
    std::copy(mInfoHash.begin(), mInfoHash.end(), std::back_inserter(handshake));
    std::copy(mClientID.begin(), mClientID.end(), std::back_inserter(handshake));

    boost::asio::async_write(
        peer.second.mSocket,
        boost::asio::buffer(handshake),
        [this, &peer] (const boost::system::error_code & error, std::size_t bytesTransferred) {
            onHandshakeSent(error, bytesTransferred, peer);
        }
    );
}

void Download::onHandshakeSent(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer) {
    if (error || bytesTransferred != peer.second.mTxBuffer.size()) {
        mPeersFromTracker.erase(peer.first);
        // TODO: handle error
        std::cout << "Failed to send handshake." << std::endl;
        return;
    }

    peer.second.mRxBuffer.resize(68);

    boost::asio::async_read(
        peer.second.mSocket,
        boost::asio::buffer(peer.second.mRxBuffer),
        [this, &peer] (const boost::system::error_code & error, std::size_t bytesTransferred) {
            onHandshakeReceived(error, bytesTransferred, peer);
        }
    );
}

void Download::onHandshakeReceived(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer) {
    if (error || bytesTransferred != peer.second.mRxBuffer.size()) {
        mPeersFromTracker.erase(peer.first);
        // TODO: handle error
        std::cout << "Failed to receive handshake." << std::endl;
        std::cout <<error.message() << std::endl;
        return;
    }

    if (peer.second.mRxBuffer.size() != 68) {
        mPeersFromTracker.erase(peer.first);
        throw std::runtime_error("RX buffer broken.");
    }

    const auto & hs = peer.second.mRxBuffer;
    // validate connection
    if (hs[0] != 19) {
        mPeersFromTracker.erase(peer.first);
        std::cout << "Invalid protocol name length." << std::endl;
        return;
    }
    if (!std::equal(PROTOCOL_NAME, PROTOCOL_NAME + 19, hs.data() + 1)) {
        mPeersFromTracker.erase(peer.first);
        std::cout << "Invalid protocol name." << std::endl;
        return;
    }
    const char * infoHash = hs.data() + 28;
    if (!std::equal(infoHash, infoHash + 20, mInfoHash.data())) {
        mPeersFromTracker.erase(peer.first);
        std::cout << "Wrong infohash." << std::endl;
        return;
    }
    const char * peerID = hs.data() + 48;
    if (!std::equal(peerID, peerID + 20, peer.first.mPeerID.data())) {
        mPeersFromTracker.erase(peer.first);
        std::cout << "Wrong peer ID." << std::endl;
        return;
    }
    if (std::equal(peerID, peerID + 20, mClientID.data())) {
        mPeersFromTracker.erase(peer.first);
        std::cout << "Client peer ID collision." << std::endl;
        return;
    }

    /*
    // actually do not check this, only mActivePeers
    for (const auto & p : mPeersFromTracker) {
        if (&p == &peer) {
            // skip itself
            continue;
        }
        if (std::equal(p.first.mPeerID.data(), p.first.mPeerID.data() + 20, peer.first.mPeerID.data())) {
            std::cout << "Duplicate connection." << std::endl;
            return;
        }
    }*/

    for (const auto & p : mActivePeers) {
        if (std::equal(p.first.mPeerID.data(), p.first.mPeerID.data() + 20, peer.first.mPeerID.data())) {
            mPeersFromTracker.erase(peer.first);
            std::cout << "Duplicate connection." << std::endl;
            return;
        }
    }

    auto peerIter = mPeersFromTracker.find(peer.first);
    if (peerIter == mPeersFromTracker.end()) {
        throw std::runtime_error("Broken Map.");
    }

    // move peer to active peers
    mActivePeers.insert(mPeersFromTracker.extract(peer.first));

    // all handshake checks passed
    std::cout << "all handshake checks passed" << std::endl;

    initReadWrite(peer);
}

void Download::initReadWrite(PeerMapNode & peer) {
    static_assert(CHAR_BIT == 8);
    const auto bitField = mPieceBitField.bitContainer();
    // implicit casting should work properly here
    if (bitField.size() >= std::numeric_limits<std::uint32_t>::max() - 1) {
        mActivePeers.erase(peer.first);
        throw std::runtime_error("Bitfield message size too large.");
    }

    peer.second.mRxBuffer.resize(4);
    boost::asio::async_read(
        peer.second.mSocket,
        boost::asio::buffer(peer.second.mRxBuffer),
        [this, &peer] (const boost::system::error_code & error, std::size_t bytesTransferred) {
            onReceiveMessageLength(error, bytesTransferred, peer);
        }
    );
    
    const std::uint32_t messageSize = bitField.size() + 1;
    auto & txBuff = peer.second.mTxBuffer;
    txBuff.resize(bitField.size() + 5);
    util::to_bytes<std::uint32_t>(messageSize, txBuff.data());
    txBuff[4] = static_cast<char>(MessageID::BITFIELD);
    std::copy(bitField.begin(), bitField.end(), txBuff.data() + 5);
    boost::asio::async_write(
        peer.second.mSocket,
        boost::asio::buffer(txBuff),
        [this, &peer](const boost::system::error_code & error, std::size_t bytesTransferred) {
            onMessageSent(error, bytesTransferred, peer);
        }
    );

    peer.second.receiveLoopActive = true;
    peer.second.sendLoopActive = true;
}

void Download::onMessageSent(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer) {
    if (error || peer.second.mTxBuffer.size() != bytesTransferred) {
        peer.second.sendLoopActive = false;
        sendReceiveFailure(peer);
        return;
    }

    const auto sendMessage = perpareNextMessageForSending(peer);

    if (sendMessage) {
        boost::asio::async_write(
            peer.second.mSocket,
            boost::asio::buffer(peer.second.mTxBuffer),
            [this, &peer] (const boost::system::error_code & error, std::size_t bytesTransferred) {
                onMessageSent(error, bytesTransferred, peer);
            }
        );
    } else {
        peer.second.mTxBuffer.resize(0);
        peer.second.mSleepTimer.expires_after(SEND_LOOP_SLEEP_TIME);
        peer.second.mSleepTimer.async_wait(
            [this, &peer] (const boost::system::error_code & error) {
                // this is a bit hacky due to possibly unexpected error type but it works
                onMessageSent(error, 0, peer);
            }
        );
    }
}

bool Download::perpareNextMessageForSending(PeerMapNode & peer) {
    auto & peerData = peer.second;
    const auto mustSendInterested = peerData.sendInterested;
    const auto mustSentHave = peerData.pieceHaveSendQueue.size() != 0;
    const auto isRequesting = peerData.is_requesting.size() != 0;

    if (mustSendInterested) {
        peerData.mTxBuffer.resize(5);
        const std::uint32_t messageSize = 1;
        util::to_bytes<std::uint32_t>(messageSize, peerData.mTxBuffer.data());
        peerData.mTxBuffer[4] = static_cast<char>(MessageID::INTERESTED);
        peerData.sendInterested = false;
        return true;
    }

    if (mustSentHave) {
        const std::size_t havesToSend = std::min<std::size_t>(
            peerData.pieceHaveSendQueue.size(),
            peerData.MAX_HAVE_MESSAGES_PER_SEND
        );
        peerData.mTxBuffer.resize(9 * havesToSend);
        const std::uint32_t messageSize = 5;
        for (std::size_t i = 0; i < havesToSend; ++i) {
            char * m = peerData.mTxBuffer.data() + 9 * i;
            util::to_bytes<std::uint32_t>(messageSize, m);
            m[4] = static_cast<char>(MessageID::HAVE);
            const std::uint32_t havePiece = peerData.pieceHaveSendQueue.front();
            peerData.pieceHaveSendQueue.pop();
            util::to_bytes<std::uint32_t>(havePiece, m + 5);
        }
        return true;
    }

    if (isRequesting) {
        const auto blockIterator = peerData.is_requesting.begin();
        const auto block = blockIterator->first;
        peerData.is_requesting.erase(blockIterator);
        std::cout << peerData.is_requesting.size() << std::endl;

        if (block.pieceIndex >= mPieceBitField.bitCount()) return false;
        if (!mPieceBitField.get(block.pieceIndex)) return false;
        const std::ptrdiff_t pieceSize =
            (block.pieceIndex == mLastPiecePosition) ?
            mLastPieceLength : mPieceLength;
        const std::ptrdiff_t offset = block.blockOffset;
        const std::ptrdiff_t size = block.blockLength;
        if (offset >= pieceSize) return false;
        if (offset + size > pieceSize) return false;
        // overflow, do more elegant checks and fix uint, int, 32bit, 64bit mixing
        if (offset + size < 0) return false; // only possible if sizeof(ptrdiff_t) == sizeof(uint32_t)
        peerData.mTxBuffer.resize(size + 13);
        char * b = peerData.mTxBuffer.data();
        util::to_bytes<std::uint32_t>(size + 9, b);
        b[4] = static_cast<char>(MessageID::PIECE);
        util::to_bytes<std::uint32_t>(block.pieceIndex, b + 5);
        util::to_bytes<std::uint32_t>(offset, b + 9);
        // mPieceLength AND NOT pieceSize !
        const std::ptrdiff_t pieceOffset = mPieceLength * block.pieceIndex;
        readFile(peerData.mTxBuffer.data() + 13, size, offset + pieceOffset);
        return true;
    }

    if (peerData.am_choking) {
        // TODO: fix that, don't just unchoke everybody
        // be nice :)
        peerData.mTxBuffer.resize(5);
        char * b = peerData.mTxBuffer.data();
        util::to_bytes<std::uint32_t>(1, b);
        b[4] = static_cast<char>(MessageID::UNCHOKE);
        peerData.am_choking = false;
        return true;
    }

    return false;
    // TODO: figure out when to send NOT_INTERESTED
    // TODO: implement
}

void Download::onReceiveMessageLength(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer) {
    if (peer.second.mRxBuffer.size() != 4) {
        peer.second.receiveLoopActive = false;
        sendReceiveFailure(peer);
        throw std::runtime_error("Broken RX buffer.");
    }
    if (error || bytesTransferred != 4) {
        std::cout << error.message() << std::endl;
        std::cout << "Failed to receive message size." << std::endl;
        peer.second.receiveLoopActive = false;
        sendReceiveFailure(peer);
        return;
    }

    const uint32_t messageLength = util::from_bytes<uint32_t>(peer.second.mRxBuffer.data());

    if (messageLength > MAX_MESSAGE_LENGTH) {
        std::cout << "Received message length too large." << std::endl;
        peer.second.receiveLoopActive = false;
        sendReceiveFailure(peer);
        return;
    }

    peer.second.mRxBuffer.resize(messageLength);

    boost::asio::async_read(
        peer.second.mSocket,
        boost::asio::buffer(peer.second.mRxBuffer),
        [this, &peer] (const boost::system::error_code & error, std::size_t bytesTransferred) {
            onReceiveMessage(error, bytesTransferred, peer);
        }
    );
}

void Download::onReceiveMessage(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer) {
    if (error || bytesTransferred != peer.second.mRxBuffer.size()) {
        std::cout << error.message() << std::endl;
        std::cout << "Failed to receive message." << std::endl;
        peer.second.receiveLoopActive = false;
        sendReceiveFailure(peer);
        return;
    }

    const auto messageOkay = processMessage(peer.second.mRxBuffer, peer);
    if (!messageOkay) {
        std::cout << "Broken message received, disconnecting." << std::endl;
        peer.second.receiveLoopActive = false;
        sendReceiveFailure(peer);
        return;
    }

    peer.second.mRxBuffer.resize(4);
    boost::asio::async_read(
        peer.second.mSocket,
        boost::asio::buffer(peer.second.mRxBuffer),
        [this, &peer] (const boost::system::error_code & error, std::size_t bytesTransferred) {
            onReceiveMessageLength(error, bytesTransferred, peer);
        }
    );
}

void Download::sendReceiveFailure(PeerMapNode & peer) {
    if (
        !peer.second.receiveLoopActive &&
        !peer.second.sendLoopActive
    ) {
        mActivePeers.erase(peer.first);
    } else {
        try {
            peer.second.mSocket.close();
        } catch(...) {
            // we don't really care here
        }
    }
}

void Download::removeFromPieceRequests(BlockRequest req) {
    // req should be valid!
    auto pieceIterator = mActivePieces.find(req.pieceIndex);
    if (pieceIterator == mActivePieces.end()) {
        // TODO: throw
        return; // hmm
    }
    auto piece = pieceIterator->second;
    const auto normalBlockSize = piece.getBlockSize(0);
    const std::uint32_t blockIndex = req.blockOffset / normalBlockSize;
    if (req.blockOffset % normalBlockSize != 0) {
        // TODO: throw
        return; // this should actually not be reachable
    }
    piece.failedGet(blockIndex);
}

bool Download::processMessage(gsl::span<const char> message, PeerMapNode & peer) {
    MessageID message_id = MessageID::INVALID;
    if (message.size() == 0) {
        message_id = MessageID::KEEP_ALIVE;
    } else if (message[0] >= 0 && message[0] <= 8) {
        message_id = static_cast<MessageID>(message[0]);
    }
    std::cout << "Received message:\n";
    std::cout << "Message type: " << messageIDToString(message_id) <<'\n';
    std::cout << "Message size: " << message.size() << std::endl;

    auto & peerData = peer.second;

    bool messageOkay = true;
    switch (message_id) {
        case MessageID::KEEP_ALIVE: {
            // noop for now
        } break; case MessageID::CHOKE: {
            if (message.size() != 1) {
                messageOkay = false;
            } else {
                peerData.is_choking = true;
                for (const auto & req : peerData.am_requesting) {
                    removeFromPieceRequests(req.first);
                }
                // TODO: be careful about it and fix the situation: requests are not reference counted,
                //       therefore, you can not request the same block from two peers
                peerData.am_requesting = {};
            }
        } break; case MessageID::UNCHOKE: {
            if (message.size() != 1) {
                messageOkay = false;
            } else {
                peerData.is_choking = false;
            }
        } break; case MessageID::INTERESTED: {
            if (message.size() != 1) {
                messageOkay = false;
            } else {
                peerData.is_interested = true;
            }
        } break; case MessageID::NOT_INTERESTED: {
            if (message.size() != 1) {
                messageOkay = false;
            } else {
                peerData.is_interested = false;
            }
        } break; case MessageID::HAVE: {
            if (message.size() != 5) {
                messageOkay = false;
            } else {
                const auto pieceIndex = util::from_bytes<std::uint32_t>(message.data() + 1);
                if (pieceIndex >= peerData.mPieceBitField.bitCount()) {
                    messageOkay = false;
                } else {
                    peerData.mPieceBitField.set(pieceIndex);
                    const auto got = mPieceBitField.get(pieceIndex);
                    if (!peerData.am_interested && !got) {
                        peerData.sendInterested = true;
                    }
                }
            }
        } break; case MessageID::BITFIELD: {
            const std::ptrdiff_t expectedMessageLength = mPieceBitField.bitContainer().size() + 1;
            if (message.size() != expectedMessageLength) {
                messageOkay = false;
                std::cout << "Wrong bit field length received." << std::endl;
            } else {
                peerData.mPieceBitField.updateContainer(message.subspan(1));
                for (std::ptrdiff_t i = 0; i < mPieceBitField.bitCount(); ++i) {
                    // if peer has something that we are missing
                    if (peerData.mPieceBitField.get(i) && !mPieceBitField.get(i)) {
                        peerData.sendInterested = true;
                        break;
                    }
                }
            }
        } break; case MessageID::REQUEST: {
            if (message.size() != 13) {
                messageOkay = false;
            } else {
                if (peerData.is_requesting.size() >= peerData.MAX_OPEN_REQUESTS) {
                    std::cout << "Too many open requests from peer." << std::endl;
                    messageOkay = false;
                } else {
                    BlockRequest req;
                    req.pieceIndex = util::from_bytes<std::uint32_t>(message.data() + 1);
                    req.blockOffset = util::from_bytes<std::uint32_t>(message.data() + 5);
                    req.blockLength = util::from_bytes<std::uint32_t>(message.data() + 9);
                    bool dummy;
                    peerData.is_requesting.emplace_get(req, dummy);
                    std::cout << peerData.is_requesting.size() << std::endl;
                }
            }
        } break; case MessageID::PIECE: {
            const auto pieceIndex = util::from_bytes<std::uint32_t>(message.data() + 1);
            const auto blockOffset = util::from_bytes<std::uint32_t>(message.data() + 5);
            auto block = message.subspan(9);
            const auto pieceFine = processPiece(peer, pieceIndex, blockOffset, block);
            if (!pieceFine) {
                messageOkay = false;
            }
        } break; case MessageID::CANCEL: {
            if (message.size() != 13) {
                messageOkay = false;
            } else {
                BlockRequest req;
                req.pieceIndex = util::from_bytes<std::uint32_t>(message.data() + 1);
                req.blockOffset = util::from_bytes<std::uint32_t>(message.data() + 5);
                req.blockLength = util::from_bytes<std::uint32_t>(message.data() + 9);
                if (peerData.is_requesting.exists(req)) {
                    peerData.is_requesting.erase(req);
                } else {
                    messageOkay = false;
                }
            }
        } break; default:
            messageOkay = false;
            break;
    }

    return messageOkay;
}

bool Download::processPiece(PeerMapNode & peer, std::uint32_t pieceIndex, std::uint32_t blockOffset, gsl::span<const char> block) {
    if (pieceIndex >= mPieceCount)
        return false;

    auto & peerData = peer.second;

    BlockRequest req;
    req.blockLength = block.size();
    const std::uint32_t blockSize = req.blockLength;
    req.blockOffset = blockOffset;
    req.pieceIndex = pieceIndex;
    if (!peerData.am_requesting.exists(req)) {
        std::cout << "Received piece that was not requested discarding, because no idea what to do with it." << std::endl;
        // TODO: detect broken / invalid blocks (blocks that were 100% never requested) to detect misbehaving peers
        return true;
    }

    peerData.am_requesting.erase(req);

    // no further integrity checks needed,
    // if the block is in peerData.am_requesting then it must be valid

    auto pieceIterator = mActivePieces.find(pieceIndex);
    if (pieceIterator == mActivePieces.end()) {
        // TODO: figure out if there needs to be something done
        std::cout << "Received piece but it is not in active pieces list, what?" << std::endl;
        return true;
    }

    auto piece = pieceIterator->second;
    const auto normalBlockSize = piece.getBlockSize(0);
    const std::uint32_t blockIndex = blockOffset / normalBlockSize;
    if (blockOffset % normalBlockSize != 0) {
        return false; // this should actually not be reachable
    }
    piece.successfulGet(blockIndex);

    const auto blockOffsetInPiece = blockOffset; // bad naming
    const auto pieceOffset = pieceIndex * mPieceLength;
    writeFile(block.data(), blockSize, pieceOffset + blockOffsetInPiece);

    if (piece.blocksMissingTotal() == 0) {
        verifyPieceAndCleanupPieceHandling(req.pieceIndex);
    }

    return true;
}

void Download::verifyPieceAndCleanupPieceHandling(std::uint32_t pieceIndex) {
    auto pieceIterator = mActivePieces.find(pieceIndex);
    auto & piece = pieceIterator->second;
    if (pieceIterator == mActivePieces.end())
        throw std::runtime_error("Piece not found.");
    if (pieceIterator->second.blocksMissingTotal() != 0)
        throw std::runtime_error("Not all piece blocks downloaded.");

    std::vector<char> pieceData;
    pieceData.resize(piece.getPieceSize());
    // careful! "mPieceLength" and not "this piece length" (last piece has a different size)
    readFile(pieceData.data(), pieceData.size(), mPieceLength * pieceIndex);

    std::array<char, SHA_DIGEST_LENGTH> hash;
    util::charSHA1(pieceData.data(), pieceData.size(), hash.data());

    if (util::equalSHA1(hash, getPieceHash(pieceIndex))) {
        // good piece
        mPieceBitField.set(pieceIndex);
        for (auto & p : mActivePeers) {
            p.second.pieceHaveSendQueue.push(pieceIndex);
        }
    } else {
        // bad piece
    }
    // erase in any case
    mActivePieces.erase(pieceIndex);
}

ConnectionDispatcher::ConnectionDispatcher(
    boost::asio::io_context &ioContext,
    std::uint16_t port,
    std::vector<std::unique_ptr<Download>> & files
) :
    mAcceptor{ ioContext },
    mFiles{ files }
{
    const tcp::endpoint endpoint{ tcp::v6(), port };

    mAcceptor.open(endpoint.protocol());
    mAcceptor.set_option(tcp::acceptor::reuse_address{ true });
    mAcceptor.bind(endpoint);
    mAcceptor.listen();
    mAcceptor.async_accept(
        [this] (
            const boost::system::error_code & error,
            tcp::socket socket
        ) {
            handleAccept(error, std::move(socket));
        }
    );
}

void ConnectionDispatcher::handleAccept(
    const boost::system::error_code & error,
    tcp::socket socket
) {
    if (!error) {
        auto peerIterator = mConnectingPeers.insert(mConnectingPeers.end(), { std::move(socket), {} });
        peerIterator->rxBuffer.resize(68);
        boost::asio::async_read(
            peerIterator->socket,
            boost::asio::buffer(peerIterator->rxBuffer),
            [this, peerIterator] (const boost::system::error_code & error, std::size_t bytesTransferred) {
                onHandshakeReceived(error, bytesTransferred, peerIterator);
            }
        );
    }

    mAcceptor.async_accept(
        [this] (const boost::system::error_code & error, tcp::socket socket) {
            handleAccept(error, std::move(socket));
        }
    );
}

void ConnectionDispatcher::onHandshakeReceived(
    const boost::system::error_code & error,
    std::size_t bytesTransferred,
    ListIter peerIterator
) {
    if (error || bytesTransferred != peerIterator->rxBuffer.size()) {
        mConnectingPeers.erase(peerIterator);
        return;
    }

    const gsl::span<const char> infoHash{ peerIterator->rxBuffer.data() + 28, 20 };

    bool found = false;
    for (auto & file : mFiles) {
        const auto fileInfoHash = file->getInfoHash();
        if (std::equal(infoHash.begin(), infoHash.end(), fileInfoHash.begin(), fileInfoHash.end())) {
            file->addPeer(std::move(peerIterator->socket), std::move(peerIterator->rxBuffer));
            found = true;
            break;
        }
    }
    if (!found) {
        std::cout << "Somebody connected and requested unknown infoHash." << std::endl;
    }

    mConnectingPeers.erase(peerIterator);
}

void Download::addPeer(
    tcp::socket && socket,
    std::vector<char> && handshake
) {
    if (handshake.size() != 68) {
        return;
    }
    const char hsLength = handshake[0];
    const gsl::span<const char> hsProtocol{ handshake.data() + 1, 19 };
    const gsl::span<const char> hsExtensions{ handshake.data() + 20, 8 };
    const gsl::span<const char> hsInfoHash{ handshake.data() + 28, 20 };
    const gsl::span<const char> hsPeerID{ handshake.data() + 48, 20 };

    if (hsLength != 19) {
        std::cout << "Adding peer: invalid handshake protocol name length." << std::endl;
        return;
    }
    if (!std::equal(hsProtocol.begin(), hsProtocol.end(), PROTOCOL_NAME, PROTOCOL_NAME + 19)) {
        std::cout << "Adding peer: invalid handshake protocol name." << std::endl;
        return;
    }
    if (!std::equal(hsInfoHash.begin(), hsInfoHash.end(), mInfoHash.begin(), mInfoHash.end())) {
        throw std::runtime_error("Peer with invalid info hash passed to Download.");
    }
    for (const auto & p : mActivePeers) {
        if (std::equal(p.first.mPeerID.begin(), p.first.mPeerID.end(), hsPeerID.begin(), hsPeerID.end())) {
            std::cout << "Duplicate connection." << std::endl;
            return;
        }
    }

    PeerKey peerKey;
    peerKey.mAddress = socket.remote_endpoint().address();
    peerKey.mPort = socket.remote_endpoint().port();
    std::copy(hsPeerID.begin(), hsPeerID.end(), peerKey.mPeerID.data());

    auto peerSearch = mPeersFromDispatcher.find(peerKey);

    if (peerSearch != mPeersFromDispatcher.end()) {
        return;
    }

    auto peerNode = mPeersFromDispatcher.emplace(
        std::piecewise_construct,
        std::forward_as_tuple(peerKey),
        std::forward_as_tuple(mIoContext, mPieceBitField.bitCount(), MAX_MESSAGE_LENGTH, MAX_MESSAGE_LENGTH)
    );

    if (peerNode.second != true) {
        // TODO: what? that should not happen
        throw std::runtime_error("Broken unordered_map ?");
    }

    // send handshake
    auto & handshake2 = peerNode.first->second.mTxBuffer;
    handshake2.clear();
    handshake2.reserve(1 + 19 + 8 + 20 + 20);
    handshake2.push_back(19);
    std::copy(PROTOCOL_NAME, PROTOCOL_NAME + 19, std::back_inserter(handshake2));
    for (int i = 0; i < 8; ++i) handshake2.push_back(0);
    std::copy(mInfoHash.begin(), mInfoHash.end(), std::back_inserter(handshake2));
    std::copy(mClientID.begin(), mClientID.end(), std::back_inserter(handshake2));

    boost::asio::async_write(
        peerNode.first->second.mSocket,
        boost::asio::buffer(handshake2),
        [this, &peer = (*peerNode.first)] (const boost::system::error_code & error, std::size_t bytesTransferred) {
            onHandshakeSentIncomingPeer(error, bytesTransferred, peer);
        }
    );
}

void Download::onHandshakeSentIncomingPeer(
    const boost::system::error_code & error,
    std::size_t bytesTransferred,
    PeerMapNode & peer
) {
    if (error || bytesTransferred != peer.second.mRxBuffer.size()) {
        std::cout << "Error sending handshake." << std::endl;
        mPeersFromDispatcher.erase(peer.first);
        return;
    }

    // move peer to active peers
    mActivePeers.insert(mPeersFromDispatcher.extract(peer.first));
    std::cout << "new peer from dispatcher" << std::endl;

    initReadWrite(peer);
}
