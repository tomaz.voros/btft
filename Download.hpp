#pragma once

#include <vector>
#include <unordered_map>
#include <boost/asio.hpp>
#include <chrono>
#include <boost/beast.hpp>
#include "bencode.hpp"
#include <array>
#include <gsl/span>
#include <fstream>
#include <unordered_map>
#include <list>
#include "util.hpp"
#include "ListMap.hpp"
#include <unordered_set>
#include <queue>

// TODO: fix naming "length" vs "size"
// TODO: fix naming "position" vs "index"

enum struct MessageID : char {
    INVALID = -2,
    KEEP_ALIVE = -1,
    CHOKE = 0,
    UNCHOKE = 1,
    INTERESTED = 2,
    NOT_INTERESTED = 3,
    HAVE = 4,
    BITFIELD = 5,
    REQUEST = 6,
    PIECE = 7,
    CANCEL = 8
};

inline std::string messageIDToString(MessageID id) {
    switch (id) {
        case MessageID::KEEP_ALIVE: return "KEEP_ALIVE";
        case MessageID::CHOKE: return "CHOKE";
        case MessageID::UNCHOKE: return "UNCHOKE";
        case MessageID::INTERESTED: return "INTERESTED";
        case MessageID::NOT_INTERESTED: return "NOT_INTERESTED";
        case MessageID::HAVE: return "HAVE";
        case MessageID::BITFIELD: return "BITFIELD";
        case MessageID::REQUEST: return "REQUEST";
        case MessageID::PIECE: return "PIECE";
        case MessageID::CANCEL: return "CANCEL";
        default: return "INVALID";
    }
}

struct PeerKey {
    // actually address and port are stored in tcp::socket and don't really need to be stored here
    boost::asio::ip::address mAddress;
    std::uint_least16_t mPort;
    std::array<char, 20> mPeerID;
};

struct BlockRequest {
    std::uint32_t pieceIndex = 0;
    std::uint32_t blockOffset = 0;
    std::uint32_t blockLength = 0;
};

namespace std {
    template<> struct hash<BlockRequest> {
        typedef BlockRequest argument_type;
        typedef std::size_t result_type;
        result_type operator () (argument_type const & a) const noexcept {
            result_type const h1 ( std::hash<std::uint32_t>{}(a.pieceIndex) );
            result_type const h2 ( std::hash<std::uint32_t>{}(a.blockOffset) );
            result_type const h3 ( std::hash<std::uint32_t>{}(a.blockLength) );
            return h1 ^ (h2 << 1) ^ (h3 >> 1);
        }
    };

    template<> struct equal_to<BlockRequest> {
        typedef BlockRequest argument_type;
        typedef bool result_type;
        result_type operator () (const argument_type & lhs, const argument_type & rhs) const noexcept {
            return
                lhs.pieceIndex  == rhs.pieceIndex  &&
                lhs.blockOffset == rhs.blockOffset &&
                lhs.blockLength == rhs.blockLength;
        }
    };
}

struct Piece {
public:
    using index_type = std::uint32_t;

    Piece(index_type pieceSize, index_type blockSize, bool isFinished = false) {
        if (blockSize < 1)
            throw std::runtime_error("blockSize can not be 0.");
        mBlockCount = pieceSize / blockSize + (pieceSize % blockSize != 0);
        mPieceSize = pieceSize;
        mBlockSize = blockSize;
        mLastBlockSize = pieceSize % blockSize;
        if (mLastBlockSize == 0)
            mLastBlockSize = mBlockSize;
        if (!isFinished)
            for (index_type i = 0; i < mBlockCount; ++i)
                mMissingBlocks.push(i);
    }

    index_type getPieceSize() const { return mPieceSize; }
    index_type getBlockCount() const { return mBlockCount; }
    index_type getBlockSize(index_type blockIndex) const {
        if (blockIndex >= mBlockCount)
            throw std::runtime_error("blockIndex out of bounds.");
        if (mBlockCount < 1) // actually never triggered because of the above exception
            return 0;
        if (blockIndex == mBlockCount - 1)
            return mLastBlockSize;
        return mBlockSize;
    }

    index_type getBlockOffset(index_type blockIndex) const {
        if (blockIndex >= mBlockCount)
            throw std::runtime_error("blockIndex out of bounds.");
        if (mBlockCount < 1) // actually never triggered because of the above exception
            return 0;
        return blockIndex * mBlockSize;
    }

    bool done() const {
        return mMissingBlocks.size() == 0 && mRequestedBlocks.size() == 0;
    }

    index_type blocksMissingNotRequested() const {
        return mMissingBlocks.size();
    }

    index_type blocksMissingRequested() const {
        return mRequestedBlocks.size();
    }

    index_type blocksMissingTotal() const {
        return blocksMissingNotRequested() + blocksMissingRequested();
    }

    index_type getNextMissing() {
        if (mMissingBlocks.empty())
            throw std::runtime_error("No more blocks to get.");
        const auto block = mMissingBlocks.front();
        mRequestedBlocks.insert(block);
        return block;
    }

    void successfulGet(index_type blockIndex) {
        if (mRequestedBlocks.find(blockIndex) == mRequestedBlocks.end())
            throw std::runtime_error("Got wrong block.");
        mRequestedBlocks.erase(blockIndex);
    }

    void failedGet(index_type blockIndex) {
        if (mRequestedBlocks.find(blockIndex) == mRequestedBlocks.end())
            throw std::runtime_error("Got wrong block.");
        mRequestedBlocks.erase(blockIndex);
        mMissingBlocks.push(blockIndex);
    }

private:
    index_type mBlockCount = 0;
    index_type mPieceSize = 0;
    index_type mBlockSize = 0;
    index_type mLastBlockSize = 0;
    std::queue<index_type> mMissingBlocks; // that is one way of doing it
    std::unordered_set<index_type> mRequestedBlocks;

};

struct Peer {
    static constexpr std::ptrdiff_t MAX_OPEN_REQUESTS = 128;
    Peer(
        boost::asio::io_context & ioContext,
        std::intptr_t pieceCount,
        std::size_t txBufferReserve,
        std::size_t rxBufferReserve
    ) :
        mSocket{ ioContext },
        mSleepTimer{ ioContext },
        mPieceBitField{ pieceCount }
    {
        mTxBuffer.reserve(txBufferReserve);
        mRxBuffer.reserve(rxBufferReserve);
    }
    boost::asio::ip::tcp::socket mSocket;
    boost::asio::steady_timer mSleepTimer;
    bool am_choking = true;
    bool am_interested = false;
    bool is_choking = true;
    bool is_interested = false;

    bool sendLoopActive = false;
    bool receiveLoopActive = false;

    bool sendInterested = false;

    std::vector<char> mTxBuffer;
    std::vector<char> mRxBuffer;

    util::bit_vector mPieceBitField;

    // char is dummy be cause I can't be bothered to implement ListSet
    ListMap<BlockRequest, char> is_requesting;
    ListMap<BlockRequest, char> am_requesting;

    static constexpr std::size_t MAX_HAVE_MESSAGES_PER_SEND = 16;
    std::queue<std::uint32_t> pieceHaveSendQueue;

};

struct PeerKeyHash {
    std::size_t operator () (const PeerKey & p) const {
        std::size_t result = 0;
        // not claiming this is any good
        for (std::size_t i = 0; i < p.mPeerID.size(); ++i)
            result = (result << 1) ^ p.mPeerID[i];
        return result;
    }
};

struct PeerKeyEqual {
    bool operator () (const PeerKey & lhs, const PeerKey & rhs) const {
        return
            lhs.mPeerID == rhs.mPeerID &&
            lhs.mPort == rhs.mPort &&
            lhs.mAddress == rhs.mAddress;
    }
};

class Download {
public:
    using PeerMap = std::unordered_map<PeerKey, Peer, PeerKeyHash, PeerKeyEqual>;
    using PeerMapNode = PeerMap::value_type;

    Download(
        boost::asio::io_context & ioContext,
        std::string_view clientID,
        const char * torrentFileName,
        std::uint_least16_t listenPort
    );

    gsl::span<const char> getInfoHash() const {
        return { mInfoHash.data(), static_cast<std::ptrdiff_t>(mInfoHash.size()) };
    }

    void addPeer(boost::asio::ip::tcp::socket && socket, std::vector<char> && handshake);

private:
    // DO NOT REALLOCATE THIS VECTOR !!! mTorrentFileView contains non owning references
    std::vector<char> mTorrentFileData;
    std::vector<bencode::view_node> mTorrentFileView;
    boost::asio::io_context & mIoContext;

    std::ptrdiff_t mAnnounceIndex = -1;
    std::ptrdiff_t mInfoIndex = -1;
    std::ptrdiff_t mPieceLengthIndex = -1;
    std::ptrdiff_t mPieceHashIndex = -1;
    std::ptrdiff_t mLengthIndex = -1;
    std::ptrdiff_t mNameIndex = -1;

    std::ptrdiff_t mPieceLength = -1;
    std::ptrdiff_t mLastPieceLength = -1;
    std::ptrdiff_t mLastPiecePosition = -1;
    std::ptrdiff_t mLength = -1;
    std::ptrdiff_t mPieceCount = -1;

    struct {
        boost::asio::ip::tcp::resolver resolver;
        boost::asio::ip::tcp::socket socket;
        boost::beast::http::request<boost::beast::http::empty_body> request;
        boost::beast::http::response<boost::beast::http::string_body> response;
        boost::beast::flat_buffer buffer;
        // DO NOT REALLOCATE THIS VECTOR !!! mTorrentFileView contains non owning references
        std::vector<char> responseBody;
        std::vector<bencode::view_node> responseView;
    } mAnnounce;

    PeerMap mPeersFromTracker;
    PeerMap mActivePeers;
    PeerMap mPeersFromDispatcher;
    std::array<char, 20> mClientID;

    std::ptrdiff_t mUploaded = 0;
    std::ptrdiff_t mDownloaded = 0;
    std::ptrdiff_t mLeft = 0;

    static constexpr std::chrono::milliseconds SEND_LOOP_SLEEP_TIME{ 100 };
    static constexpr const char * PROTOCOL_NAME = "BitTorrent protocol";
    static constexpr std::uint32_t MAX_MESSAGE_LENGTH = 256 * 1024;
    static constexpr std::uint32_t REQUEST_BLOCK_SIZE = 16 * 1024;

    static constexpr std::ptrdiff_t MAX_NUM_WANT = 5;
    std::uint_least16_t mListenPort = 0;
    std::array<char, 20> mInfoHash;
    std::array<char, 60> mInfoHashPercent;

    std::fstream mFile;
    util::bit_vector mPieceBitField;

    std::unordered_map<std::uint32_t, Piece> mActivePieces;

    void scheduleAnnounce();
    void onResolveAnnounce(
        const boost::system::error_code & error,
        boost::asio::ip::tcp::resolver::results_type results
    );

    void onConnectAnnounce(const boost::system::error_code & error, boost::asio::ip::tcp::resolver::results_type::const_iterator i);
    void onWriteAnnounce(const boost::system::error_code & ec, std::size_t bytesTransferred);
    void onReadAnnounce(const boost::system::error_code & ec, std::size_t bytesTransferred);
    void schedulePeerConnect(PeerMapNode & peer);
    void onPeerConnect(boost::system::error_code ec, PeerMapNode & peer);
    void onHandshakeSent(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer);
    void onHandshakeReceived(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer);
    void onReceiveMessageLength(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer);
    void onReceiveMessage(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer);
    // returns true if the message is valid
    bool processMessage(gsl::span<const char> message, PeerMapNode & peer);
    void onHandshakeSentIncomingPeer(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer);
    void sendBitFieldAndStartMessaging();
    void onMessageSent(const boost::system::error_code & error, std::size_t bytesTransferred, PeerMapNode & peer);
    // returns false if the message is invalid and we should sleep instead
    bool perpareNextMessageForSending(PeerMapNode & peer);

    // helpers
    gsl::span<const char> getPieceHash(std::ptrdiff_t pieceIndex);
    void writeFile(const char * data, std::ptrdiff_t byteCount, std::ptrdiff_t byteOffset);
    void readFile(char * data, std::ptrdiff_t byteCount, std::ptrdiff_t byteOffset);
    // creates the file with all invalid hash values
    void setupNewFile();
    void setupExistingFile();
    void initReadWrite(PeerMapNode & peer);
    void sendReceiveFailure(PeerMapNode & peer);
    bool processPiece(PeerMapNode & peer, std::uint32_t pieceIndex, std::uint32_t blockOffset, gsl::span<const char> block);
    void removeFromPieceRequests(BlockRequest req);
    void verifyPieceAndCleanupPieceHandling(std::uint32_t pieceIndex);

};

class ConnectionDispatcher {
public:
    ConnectionDispatcher(
        boost::asio::io_context & ioContext,
        std::uint16_t port,
        std::vector<std::unique_ptr<Download>> & files
    );

private:
    boost::asio::ip::tcp::acceptor mAcceptor;
    std::vector<std::unique_ptr<Download>> & mFiles;

    struct TmpSocketStorage {
        boost::asio::ip::tcp::socket socket;
        std::vector<char> rxBuffer;
    };
    // TODO: limit the max list size
    std::list<TmpSocketStorage> mConnectingPeers;
    using ListIter = decltype(mConnectingPeers)::iterator;

    void handleAccept(const boost::system::error_code & error, boost::asio::ip::tcp::socket socket);
    void onHandshakeReceived(const boost::system::error_code & error, std::size_t bytesTransferred, ListIter peerIterator);

};
