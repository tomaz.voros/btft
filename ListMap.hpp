#pragma once

#include <cassert>
#include <list>
#include <unordered_map>

template <typename Key, typename Val>
class ListMap {
public:
    template <typename ... Args>
    Val & emplace(const Key & key, Args && ... args) {
        mList.emplace_back(
            std::piecewise_construct,
            std::forward_as_tuple(key),
            std::forward_as_tuple(std::forward<Args>(args)...)
        );
        const auto result = mMap.emplace(key, --mList.end());
        assert(result.second && "Item already exists.");
        return mList.back().second;
    }

    Val & get(const Key & key) {
        const auto i = mMap.find(key);
        assert(i != mMap.end());
        return i->second->second;
    }

    template <typename ... Args>
    Val & emplace_get(const Key & key, bool & isCached, Args && ... args) {
        isCached = mMap.find(key) != mMap.end();
        if (isCached) {
            return get(key);
        } else {
            return emplace(key, std::forward<Args>(args)...);
        }
    }

    bool exists(const Key & key) {
        return mMap.find(key) != mMap.end();
    }

    typename std::list<std::pair<Key, Val>>::iterator begin() {
        return mList.begin();
    }

    typename std::list<std::pair<Key, Val>>::iterator end() {
        return mList.end();
    }

    typename std::list<std::pair<Key, Val>>::iterator erase(const typename std::list<std::pair<Key, Val>>::iterator & it) {
        mMap.erase(it->first);
        return mList.erase(it);
    }

    // return true if erased
    void erase(const Key & key) {
        const auto mapIterator = mMap.find(key);
        assert(mapIterator != mMap.end());
        mList.erase(mapIterator->second);
        mMap.erase(mapIterator);
    }

    std::size_t size() const { return mList.size(); }

private:
    std::list<std::pair<Key, Val>> mList;
    std::unordered_map<Key, typename decltype(mList)::iterator> mMap;

};
