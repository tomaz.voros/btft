# btft

This is a simple implementation of a basic BitTorrent client. The goal of this project was for me to see how much code would be needed to implement a working BitTorrent client.

As of 23.4.2019 the client is still not complete. The client is able to find peers via a tracker or accept incoming peer connections and seed files to them. Downloading of files is about 100 lines of code away from being implemented :).

The client uses a single thread for everything with boost::asio for asynchronous networking.
