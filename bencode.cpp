#include "bencode.hpp"

#include "catch/catch.hpp"

#include <vector>
#include <limits>
#include <cstring>

#include "util.hpp"

namespace {
    static constexpr const char * INVALID_DATA_STRING = "Invalid data.";
    static constexpr const char * OVERFLOW_STRING = "Index overflow.";
    static constexpr const char * STRING_LENGTH_UNSUPPORTED_STRING = "String length not supported.";
    static constexpr const char * DICTIONARY_DUPLICATE_KEY_STRING = "Dictionary contains a duplicate.";
    static constexpr const char * DICTIONARY_WRONG_KEY_ORDER_STRING = "Dictionary keys are not sorted.";

    std::tuple<util::safe_pointer<const char>, std::ptrdiff_t> decodeAny(util::safe_pointer<const char> it, std::vector<bencode::index_node> & result);
    std::tuple<util::safe_pointer<const char>, bencode::index_node> decodeInteger(util::safe_pointer<const char> it);
    std::tuple<util::safe_pointer<const char>, bencode::index_node> decodeString(util::safe_pointer<const char> it);
    std::tuple<util::safe_pointer<const char>, std::ptrdiff_t> decodeList(util::safe_pointer<const char> it, std::vector<bencode::index_node> & result);
    std::tuple<util::safe_pointer<const char>, std::ptrdiff_t> decodeDictionary(util::safe_pointer<const char> it, std::vector<bencode::index_node> & result);

    std::tuple<util::safe_pointer<const char>, std::ptrdiff_t> decodeAny(util::safe_pointer<const char> it, std::vector<bencode::index_node> & result) {
        if (*it == 'i') {
            // TODO: set integer node.next
            auto [new_it, node] = decodeInteger(it);
            result.push_back(node);
            if (std::numeric_limits<std::ptrdiff_t>::max() < result.size() - 1) {
                throw std::runtime_error(OVERFLOW_STRING);
            }
            return { new_it, result.size() - 1 };
        } else if (*it >= '0' && *it <= '9') {
            auto [new_it, node] = decodeString(it);
            result.push_back(node);
            if (std::numeric_limits<std::ptrdiff_t>::max() < result.size() - 1) {
                throw std::runtime_error(OVERFLOW_STRING);
            }
            return { new_it, result.size() - 1 };
        } else if (*it == 'l') {
            return decodeList(it, result);
        } else if (*it == 'd') {
            return decodeDictionary(it, result);
        } else {
            throw std::runtime_error(INVALID_DATA_STRING);
        }
    }

    std::tuple<util::safe_pointer<const char>, bencode::index_node> decodeInteger(util::safe_pointer<const char> it) {
        bencode::index_node this_node;
        this_node.node_type = bencode::type::INTEGER;
        if (*it != 'i') throw std::runtime_error(INVALID_DATA_STRING);
        ++it;
        this_node.data_begin = it.get_ptr() - it.get_begin();
        // special case: number is 0
        if (*it == '0') {
            ++it;
            this_node.data_end = it.get_ptr() - it.get_begin();
            if (*it != 'e') {
                throw std::runtime_error(INVALID_DATA_STRING);
            }
            ++it;
            return { it, this_node };
        }
        // special case: number is negative
        if (*it == '-') {
            ++it;
            if (*it < '1' || *it > '9') {
                throw std::runtime_error(INVALID_DATA_STRING);
            }
        }
        // if empty data
        if (*it < '0' || *it > '9')
            throw std::runtime_error(INVALID_DATA_STRING);
        while (*it >= '0' && *it <= '9') {
            ++it;
        }
        if (*it != 'e') {
            throw std::runtime_error(INVALID_DATA_STRING);
        }
        this_node.data_end = it.get_ptr() - it.get_begin();
        ++it;
        return { it, this_node };
    }

    std::tuple<util::safe_pointer<const char>, bencode::index_node> decodeString(util::safe_pointer<const char> it) {
        // get string length
        const char * string_length_begin = it.get_ptr();
        if (*it == '0') {
            ++it;
        } else {
            while (*it >= '0' && *it <= '9') {
                ++it;
            }
        }
        const char * string_length_end = it.get_ptr();
        const auto length_string_length = string_length_end - string_length_begin;
        if (length_string_length == 0)
            throw std::runtime_error(INVALID_DATA_STRING);
        if (*it != ':')
            throw std::runtime_error(INVALID_DATA_STRING);
        ++it;
        using length_type = std::ptrdiff_t;
        if (length_string_length > std::numeric_limits<length_type>::digits10)
            throw std::runtime_error(STRING_LENGTH_UNSUPPORTED_STRING);
        length_type string_length = 0;
        for (const char * j = string_length_begin; j != string_length_end; ++j) {
            string_length *= 10;
            string_length += *j - '0';
        }
        bencode::index_node this_node;
        this_node.node_type = bencode::type::STRING;
        // get string
        this_node.data_begin = it.get_ptr() - it.get_begin();
        // do not "optimize out" the next line, it does range checking behind the scenes
        it += string_length;
        this_node.data_end = it.get_ptr() - it.get_begin();
        return { it, this_node };
    }

    std::tuple<util::safe_pointer<const char>, std::ptrdiff_t> decodeList(util::safe_pointer<const char> it, std::vector<bencode::index_node> & result) {
        if (*it != 'l') {
            throw std::runtime_error(INVALID_DATA_STRING);
        }
        ++it;
        const std::ptrdiff_t list_data_begin_index = it.get_ptr() - it.get_begin();
        if (std::numeric_limits<std::ptrdiff_t>::max() < result.size()) {
            throw std::runtime_error(OVERFLOW_STRING);
        }
        const std::ptrdiff_t list_index = result.size();
        result.emplace_back();
        result[list_index].node_type = bencode::type::LIST;
        std::ptrdiff_t previous_index = -1;
        while (*it != 'e') {
            auto [new_it_2, node_index] = decodeAny(it, result);
            it = new_it_2;
            if (previous_index == -1) {
                result[list_index].child_index = node_index;
            } else {
                result[previous_index].next_index = node_index;
            }
            previous_index = node_index;
        }
        const std::ptrdiff_t list_data_end_index = it.get_ptr() - it.get_begin();
        ++it;
        result[list_index].data_begin = list_data_begin_index;
        result[list_index].data_end = list_data_end_index;
        return { it, list_index };
    }

    std::tuple<util::safe_pointer<const char>, std::ptrdiff_t> decodeDictionary(util::safe_pointer<const char> it, std::vector<bencode::index_node> & result) {
        if (*it != 'd') {
            throw std::runtime_error(INVALID_DATA_STRING);
        }
        ++it;
        const std::ptrdiff_t dictionary_data_begin_index = it.get_ptr() - it.get_begin();
        if (std::numeric_limits<std::ptrdiff_t>::max() < result.size()) {
            throw std::runtime_error(OVERFLOW_STRING);
        }
        const std::ptrdiff_t dict_index = result.size();
        result.emplace_back();
        result[dict_index].node_type = bencode::type::DICTIONARY;
        std::ptrdiff_t previous_index = -1;
        while (*it != 'e') {
            auto [new_it, key_node] = decodeString(it);
            if (previous_index != -1) {
                // check key uniqueness and order
                const std::ptrdiff_t this_key_length = key_node.data_end - key_node.data_begin;
                const std::ptrdiff_t last_key_length = result[previous_index].key_end - result[previous_index].key_begin;
                const auto comp_result = std::memcmp(
                    it.get_begin() + result[previous_index].key_begin,
                    it.get_begin() + key_node.data_begin,
                    std::min(last_key_length, this_key_length)
                );
                if (comp_result < 0) {
                    // okay
                } else if (comp_result == 0) {
                    if (last_key_length < this_key_length) {
                        // okay
                    } else if (last_key_length == this_key_length) {
                        throw std::runtime_error(DICTIONARY_DUPLICATE_KEY_STRING);
                    } else {
                        throw std::runtime_error(DICTIONARY_WRONG_KEY_ORDER_STRING);
                    }
                } else {
                    throw std::runtime_error(DICTIONARY_WRONG_KEY_ORDER_STRING);
                }
            }
            it = new_it;
            auto [new_it_2, node_index] = decodeAny(it, result);
            it = new_it_2;
            result[node_index].key_begin = key_node.data_begin;
            result[node_index].key_end = key_node.data_end;
            if (previous_index == -1) {
                result[dict_index].child_index = node_index;
            } else {
                result[previous_index].next_index = node_index;
            }
            previous_index = node_index;
        }
        const std::ptrdiff_t dictionary_data_end_index = it.get_ptr() - it.get_begin();
        ++it;
        result[dict_index].data_begin = dictionary_data_begin_index;
        result[dict_index].data_end = dictionary_data_end_index;
        return { it, dict_index };
    }

    std::string_view bruttoViewInteger(
        const std::vector<bencode::view_node> & view,
        const std::ptrdiff_t nodeIndex
    ) {
        if (nodeIndex < 0 || nodeIndex >= view.size())
            throw std::runtime_error("Inalid node index.");
        if (view[nodeIndex].node_type != bencode::type::INTEGER)
            throw std::runtime_error("Inalid node type.");
        return std::string_view(
            view[nodeIndex].data.data() - 1,
            view[nodeIndex].data.size() + 2
        );
    }

    std::string_view bruttoViewString(
        const std::vector<bencode::view_node> & view,
        const std::ptrdiff_t nodeIndex
    ) {
        if (nodeIndex < 0 || nodeIndex >= view.size())
            throw std::runtime_error("Inalid node index.");
        if (view[nodeIndex].node_type != bencode::type::STRING)
            throw std::runtime_error("Inalid node type.");
        std::ptrdiff_t string_size_tmp = view[nodeIndex].data.size();
        std::ptrdiff_t length = 1;
        while (string_size_tmp /= 10) length++;
        return std::string_view(
            view[nodeIndex].data.data() - 1 - length,
            view[nodeIndex].data.size() + 1 + length
        );
    }

    std::string_view bruttoViewDictionary(
        const std::vector<bencode::view_node> & view,
        const std::ptrdiff_t nodeIndex
    ) {
        if (nodeIndex < 0 || nodeIndex >= view.size())
            throw std::runtime_error("Inalid node index.");
        if (view[nodeIndex].node_type != bencode::type::DICTIONARY)
            throw std::runtime_error("Inalid node type.");
        return std::string_view(
            view[nodeIndex].data.data() - 1,
            view[nodeIndex].data.size() + 2
        );
    }

    std::string_view bruttoViewList(
        const std::vector<bencode::view_node> & view,
        const std::ptrdiff_t nodeIndex
    ) {
        if (nodeIndex < 0 || nodeIndex >= view.size())
            throw std::runtime_error("Inalid node index.");
        if (view[nodeIndex].node_type != bencode::type::LIST)
            throw std::runtime_error("Inalid node type.");
        return std::string_view(
            view[nodeIndex].data.data() - 1,
            view[nodeIndex].data.size() + 2
        );
    }
}

namespace bencode {
    std::string_view bruttoView(
        const std::vector<view_node> & view,
        const std::ptrdiff_t nodeIndex
    ) {
        if (nodeIndex < 0 || nodeIndex >= view.size())
            throw std::runtime_error("Inalid node index.");
        switch (view[nodeIndex].node_type) {
            case type::INTEGER: return bruttoViewInteger(view, nodeIndex);
            case type::STRING: return bruttoViewString(view, nodeIndex);
            case type::DICTIONARY: return bruttoViewDictionary(view, nodeIndex);
            case type::LIST: return bruttoViewList(view, nodeIndex);
            default: throw std::runtime_error("Inalid node type.");
        }
    }

    std::vector<index_node> decode(const char * begin, const char * end) {
        try {
            std::vector<index_node> result;
            util::safe_pointer<const char> it(begin, end);
            auto [it_new, node_index] = decodeAny(it, result);
            it = it_new;
            if (it.get_ptr() != end) {
                throw std::runtime_error(INVALID_DATA_STRING);
            }
            return result;
        } catch (...) {
            return {};
        }
    }

    std::vector<view_node> indexNodeToViewNode(
        const char * bencodeData,
        const std::vector<index_node> & in
    ) {
        std::vector<view_node> result;
        result.reserve(in.size());
        for (const auto & node : in) {
            view_node new_node;
            new_node.node_type = node.node_type;
            new_node.next_index = node.next_index;
            new_node.child_index = node.child_index;
            if (node.data_begin != -1) {
                assert(node.data_end != -1);
                new_node.data = { bencodeData + node.data_begin, static_cast<std::size_t>(node.data_end - node.data_begin) };
            }
            if (node.key_begin != -1) {
                assert(node.key_end != -1);
                new_node.key = { bencodeData + node.key_begin, static_cast<std::size_t>(node.key_end - node.key_begin) };
            }
            result.emplace_back(new_node);
        }
        return result;
    }

    std::ptrdiff_t findKey(
        const std::vector<view_node> & view,
        const std::ptrdiff_t dictIndex,
        const std::string_view & key
    ) {
        if (dictIndex >= view.size() || dictIndex < 0)
            return -1;
        if (view[dictIndex].node_type != type::DICTIONARY)
            return -1;
        std::ptrdiff_t result = view[dictIndex].child_index;
        while (result != -1) {
            assert(result >= 0 && result <= view.size());
            if (view[result].key == key) {
                break;
            } else {
                result = view[result].next_index;
            }
        }
        return result;
    }


    std::string_view keyView(const char * bencodeData, const index_node & node) {
        if (node.key_begin < 0) {
            assert(node.key_end < 0);
            return { nullptr, 0 };
        }
        return { bencodeData + node.key_begin, static_cast<std::size_t>(node.key_end - node.key_begin) };
    }

    std::string_view dataView(const char * bencodeData, const index_node & node) {
        if (node.data_begin < 0) {
            assert(node.data_end < 0);
            return { nullptr, 0 };
        }
        return { bencodeData + node.data_begin, static_cast<std::size_t>(node.data_end - node.data_begin) };
    }

}

TEST_CASE("bencode decode integer", "[bencode][integer]") {
    struct TestData {
        const char * begin = nullptr;
        const char * end = nullptr;
        const char * correctEnd = nullptr;
        bool valid = false;
        const char * correctValue = nullptr;
    };
    const char * strings[] {
        "i3e333333",
        "i19e33333",
        "i0e333333",
        "i00e33333",
        "i-5e33333",
        "i-299e333",
        "i1234xyzw",
        "ie1234567",
        "i-exxxxxx",
        "i-0eyyyyy",
    };
    std::vector<TestData> testData{
        { nullptr       , nullptr       , nullptr       , false, nullptr }, // empty buffer
        { strings[0]    , strings[0] + 9, strings[0] + 3, true , "3"     }, // large buffer correct
        { strings[0]    , strings[0] + 3, strings[0] + 3, true , "3"     }, // snug buffer
        { strings[0] + 1, strings[0] + 9, nullptr       , false, nullptr },    // missing i
        { strings[0]    , strings[0] + 2, nullptr       , false, nullptr },    // missing e
        { strings[1]    , strings[1] + 9, strings[1] + 4, true , "19"    }, // 2 digit large buffer
        { strings[1]    , strings[1] + 4, strings[1] + 4, true , "19"    }, // 2 digit snug buffer
        { strings[2]    , strings[2] + 9, strings[2] + 3, true , "0"     }, // zero large buffer
        { strings[2]    , strings[2] + 3, strings[2] + 3, true , "0"     }, // zero snug
        { strings[3]    , strings[3] + 9, nullptr       , false, nullptr }, // 2x zero
        { strings[4]    , strings[4] + 9, strings[4] + 4, true , "-5"    }, // negative 1 digit
        { strings[5]    , strings[5] + 9, strings[5] + 6, true , "-299"  }, // negative 3 digit
        { strings[6]    , strings[6] + 9, nullptr       , false, nullptr }, // missing e large buffer
        { strings[6]    , strings[6] + 9, nullptr       , false, nullptr }, // missing e snug buffer
        { strings[7]    , strings[7] + 9, nullptr       , false, nullptr }, // missing number
        { strings[8]    , strings[8] + 9, nullptr       , false, nullptr }, // missing number after "-"
        { strings[9]    , strings[9] + 9, nullptr       , false, nullptr }, // negative zero
    };
    for (const auto & testSample : testData) {
        const util::safe_pointer<const char> it(testSample.begin, testSample.end);
        if (testSample.valid) {
            auto [s_ptr, node] = decodeInteger(it);
            REQUIRE(s_ptr.get_ptr() == testSample.correctEnd);
            const std::ptrdiff_t result_length = node.data_end - node.data_begin;
            // careful using std::strlen !!!
            const std::ptrdiff_t correct_result_length = std::strlen(testSample.correctValue);
            REQUIRE(result_length == correct_result_length);
            REQUIRE(std::memcmp(testSample.correctValue, it.get_begin() + node.data_begin, result_length) == 0);
        } else {
            REQUIRE_THROWS(decodeInteger(it));
        }
    }
}

TEST_CASE("bencode decode string", "[bencode][string]") {
    struct TestData {
        const char * begin = nullptr;
        const char * end = nullptr;
        const char * correctEnd = nullptr;
        bool valid = false;
        const char * correctValue;
    };
    const char * strings[] {
        "4:banaxyz",
        "4banaxyzw",
        "12:0123456789abxxx",
        "12x0123456789abxxxy",
        "00:asd",
        "-2:asd",
    };
    std::vector<TestData> testData{
        { nullptr       , nullptr        , nullptr        , false, nullptr        }, // empty buffer
        { strings[0] + 2, strings[0] +  9, nullptr        , false, nullptr        }, // garbage
        { strings[0]    , strings[0] +  9, strings[0] + 6 , true , "bana"       }, // large buffer correct
        { strings[0]    , strings[0] +  6, strings[0] + 6 , true , "bana"       }, // snug buffer
        { strings[0] + 1, strings[0] +  9, nullptr        , false, nullptr        },    // missing length
        { strings[1]    , strings[1] +  9, nullptr        , false, nullptr        },    // missing :
        { strings[2]    , strings[2] + 18, strings[2] + 15, true , "0123456789ab" },    // 2 digits length
        { strings[2]    , strings[2] + 14, nullptr        , false, nullptr        },    // 2 digits length past the end access
        { strings[3]    , strings[3] + 18, nullptr        , false, nullptr        },    // 2 digits length, missing :
        { strings[4] + 1, strings[4] +  6, strings[4] + 3 , true , ""             },    // 0 length
        { strings[4]    , strings[4] +  6, nullptr        , false, nullptr        },    // invalid length 00
        { strings[5]    , strings[5] +  6, nullptr        , false, nullptr        },    // invalid length -2
    };
    for (const auto & testSample : testData) {
        const util::safe_pointer<const char> it(testSample.begin, testSample.end);
        if (testSample.valid) {
            auto [s_ptr, node] = decodeString(it);
            REQUIRE(s_ptr.get_ptr() == testSample.correctEnd);
            const std::ptrdiff_t result_length = node.data_end - node.data_begin;
            // careful using std::strlen !!!
            const std::ptrdiff_t correct_result_length = std::strlen(testSample.correctValue);
            REQUIRE(result_length == correct_result_length);
            REQUIRE(std::memcmp(testSample.correctValue, it.get_begin() + node.data_begin, result_length) == 0);
        } else {
            REQUIRE_THROWS(decodeInteger(it));
        }
    }
}
