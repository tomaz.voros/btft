#pragma once

#include <string_view>
#include <vector>

namespace bencode {
    enum struct type { INTEGER, STRING, LIST, DICTIONARY, INVALID };
    struct index_node {
        type node_type = type::INVALID;
        // valid only if parent_node == type::LIST || parent_node == type::DICTIONARY
        std::ptrdiff_t next_index = -1;
        // valid only if node_type == type::LIST || node_type == type::DICTIONARY
        std::ptrdiff_t child_index = -1;
        // valid only if node_type == type::INTEGER || node_type == type::STRING
        std::ptrdiff_t data_begin = -1;
        // valid only if node_type == type::INTEGER || node_type == type::STRING
        std::ptrdiff_t data_end = -1;
        // valid only if parent_node == type::DICTIONARY
        std::ptrdiff_t key_begin = -1;
        // valid only if parent_node == type::DICTIONARY
        std::ptrdiff_t key_end = -1;
    };

    struct view_node {
        type node_type = type::INVALID;
        // valid only if parent_node == type::LIST || parent_node == type::DICTIONARY
        std::ptrdiff_t next_index = -1;
        // valid only if node_type == type::LIST || node_type == type::DICTIONARY
        std::ptrdiff_t child_index = -1;
        // valid only if node_type == type::INTEGER || node_type == type::STRING
        std::string_view data = { nullptr, 0 };
        // valid only if parent_node == type::DICTIONARY
        std::string_view key = { nullptr, 0 };
    };

    std::vector<index_node> decode(
        const char * begin,
        const char * end
    );

    std::vector<view_node> indexNodeToViewNode(
        const char * bencodeData,
        const std::vector<index_node> & in
    );

    /**
     * Finds the index of the child node with key #key.
     * @return -1 if no child node with key #key found. Also -1 if
     *         dictIndex is out of bounds or if view[dictIndex]
     *         is not of type type::DICTIONARY.
     */
    std::ptrdiff_t findKey(
        const std::vector<view_node> & view,
        const std::ptrdiff_t dictIndex,
        const std::string_view & key
    );
    std::string_view keyView(const char * bencodeData, const index_node & node);
    std::string_view dataView(const char * bencodeData, const index_node & node);

    std::string_view bruttoView(
        const std::vector<view_node> & view,
        const std::ptrdiff_t nodeIndex
    );

}
