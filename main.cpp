#include <vector>
#include <iostream>

#include <boost/asio.hpp>
#include "Download.hpp"

#include <array>

int main(int argc, const char * argv[]) {
    static constexpr std::array<char, 20> CLIENT_PEER_ID{
        '-', 't', 'e', 's', 't', '1', '2', '-', 'a', 's',
        'd', 'f', '1', '2', '3', 'g', 'h', 'j', 'k', '5'
    };
    static constexpr std::uint_least16_t LISTEN_PORT = 6885;

    boost::asio::io_context ioContext;
    std::vector<std::unique_ptr<Download>> files;

    for (int i = 1; i < argc; ++i)
        files.emplace_back(
            std::make_unique<Download>(
                ioContext,
                std::string_view{ CLIENT_PEER_ID.data(), CLIENT_PEER_ID.size() },
                argv[i],
                LISTEN_PORT
            )
        );

    ConnectionDispatcher connectionDispatcher(ioContext, LISTEN_PORT, files);
    ioContext.run();

    std::cout << "\n\n----\n\n" << std::endl;
}
