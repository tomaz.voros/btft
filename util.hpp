#pragma once

#include <vector>
#include <fstream>
#include <climits>
#include <string_view>
#include <openssl/sha.h>
#include <gsl/span>
#include <list>
#include <unordered_set>

namespace util {
    template <typename T> T from_bytes(const char * in);
    template <typename T> void to_bytes(const T & in, char * out);

    template <> inline std::uint32_t from_bytes(const char * in) {
        static_assert(CHAR_BIT == 8);
        std::uint32_t out = 0;
        out |= (static_cast<std::uint32_t>(in[0]) & 0xffu) << 24u;
        out |= (static_cast<std::uint32_t>(in[1]) & 0xffu) << 16u;
        out |= (static_cast<std::uint32_t>(in[2]) & 0xffu) <<  8u;
        out |= (static_cast<std::uint32_t>(in[3]) & 0xffu)       ;
        return out;
    }

    template <typename T, typename U>
    inline bool equalSHA1(const T & a, const U & b) {
        if (a.size() != SHA_DIGEST_LENGTH)
            throw std::runtime_error("Invalid SHA1 length.");
        if (b.size() != SHA_DIGEST_LENGTH)
            throw std::runtime_error("Invalid SHA1 length.");
        return std::equal(a.begin(), a.end(), b.begin(), b.end());
    };

    inline void charSHA1(const char * data, std::size_t size, char * hash) {
        SHA1(
            reinterpret_cast<const unsigned char *>(data),
            size,
            reinterpret_cast<unsigned char *>(hash)
        );
    };

    template <> inline void to_bytes(const std::uint32_t & in, char * out) {
        static_assert(CHAR_BIT == 8);
        out[0] = static_cast<char>(in >> 24u & 0xffu);
        out[1] = static_cast<char>(in >> 16u & 0xffu);
        out[2] = static_cast<char>(in >>  8u & 0xffu);
        out[3] = static_cast<char>(in        & 0xffu);
    }

    inline std::vector<char> loadFile(const char * fileName) {
        std::ifstream file{ fileName, std::ifstream::binary | std::ifstream::ate };
        if (!file.good()) return {};
        const std::ptrdiff_t fileSize = file.tellg();
        if (!file.good()) return {};
        std::vector<char> fileData(fileSize);
        file.seekg(0);
        if (!file.good()) return {};
        file.read(reinterpret_cast<char *>(fileData.data()), fileData.size());
        if (!file.good()) return {};
        return fileData;
    }

    template <typename T>
    class safe_pointer {
    public:
        static constexpr const char * ERROR_MESSAGE = "Out of range.";

        safe_pointer(T * begin_, T * end_) :
            mBegin{ begin_ }, mPtr{ begin_ }, mEnd{ end_ }
        {
            if (mPtr > mEnd)
                throw std::runtime_error(ERROR_MESSAGE);
            if (mPtr == nullptr && mEnd != nullptr)
                throw std::runtime_error(ERROR_MESSAGE);
        }

        safe_pointer<T> & operator ++ () {
            if (mPtr == mEnd)
                throw std::runtime_error(ERROR_MESSAGE);
            ++mPtr;
            return *this;
        }

        safe_pointer<T> & operator -- () {
            if (mPtr == mBegin)
                throw std::runtime_error(ERROR_MESSAGE);
            --mPtr;
            return *this;
        }

        T & operator * () {
            if (mPtr == mEnd)
                throw std::runtime_error(ERROR_MESSAGE);
            return *mPtr;
        }

        safe_pointer<T> & operator += (const std::ptrdiff_t length) {
            const std::ptrdiff_t offset = mBegin - mPtr;
            if (length < offset || length > mEnd - mPtr)
                throw std::runtime_error(ERROR_MESSAGE);
            mPtr += length;
            return *this;
        }

        T * get_begin() const { return mBegin; }
        T * get_ptr() const { return mPtr; }
        T * get_end() const { return mEnd; }

    private:
        T * mBegin;
        T * mPtr;
        T * mEnd;

    };

    struct split_uri {
        std::string_view scheme;
        std::string_view host;
        std::string_view port;
        std::string_view target;
    };

    // this function will not detect all valid and invalid URIs
    inline split_uri parseURI(const std::string_view uri) {
        try {
            split_uri result;
            safe_pointer<const char> p(uri.begin(), uri.end());

            while (*p != ':') ++p;
            if (*(++p) != '/') throw std::runtime_error("Invalid URI.");
            if (*(++p) != '/') throw std::runtime_error("Invalid URI.");
            p += -2;
            result.scheme = std::string_view(p.get_begin(), p.get_ptr() - p.get_begin());
            p += 3;
            const char * host_begin = p.get_ptr();
            // find beginning of path
            while (*p != '/') ++p;
            result.target = uri.substr(p.get_ptr() - p.get_begin());
            // move back to check for port
            try {
                safe_pointer<const char> p_tmp = p;
                while (true) {
                    --p_tmp;
                    if (*p_tmp >= '0' && *p_tmp <= '9') {
                        continue;
                    } else if (*p_tmp == ':') {
                        break;
                    } else {
                        throw std::runtime_error("Missing port");
                    }
                }
                ++p_tmp;
                result.port = std::string_view(p_tmp.get_ptr(), p.get_ptr() - p_tmp.get_ptr());
                --p_tmp;
                result.host = std::string_view(host_begin, p_tmp.get_ptr() - host_begin);
            } catch (...) {
                result.host = std::string_view(host_begin, p.get_ptr() - host_begin);
            }
            return result;
        } catch (...) {
            return {};
        }
    }

    inline std::string_view toPercentEncoding(std::string_view in, char * out, size_t outSize) {
        if (in.size() > 0 && 3 > std::numeric_limits<decltype(in.size())>::max() / in.size()) {
            throw std::runtime_error("Size verflow.");
        }
        if (outSize < 3 * in.size()) {
            throw std::runtime_error("Out buffer too small.");
        }
        char * outIt = out;
        for (const auto & c : in) {
            const int hi = c >> 4 & 0xf;
            const int lo = c      & 0xf;
            *outIt++ = '%';
            *outIt++ = hi > 9 ? hi + 'a' - 10 : hi + '0';
            *outIt++ = lo > 9 ? lo + 'a' - 10 : lo + '0';
        }
        return std::string_view(out, in.size() * 3);
    }

    // std::bitfield and std::vector<bool> are not suitable :(
    // using char as container for simplicity
    class bit_vector {
        static_assert(CHAR_BIT == 8);
    public:
        bit_vector() {}
        bit_vector(std::ptrdiff_t size) {
            if (size < 0) {
                throw std::runtime_error("Invalid size argument in bit_vector.");
            }
            const std::ptrdiff_t charCount = (size + CHAR_BIT - 1) / CHAR_BIT;
            if (charCount < 0) {
                throw std::runtime_error("Integer overflow in bit_vector.");
            }
            mBits.resize(charCount, 0);
            mBitCount = size;
        }

        std::ptrdiff_t bitCount() const {
            return mBitCount;
        }

        bool get(std::ptrdiff_t index) const {
            if (index < 0 || index >= mBitCount) {
                throw std::runtime_error("Out of bounds access in bit_vector.");
            }
            const char shift = CHAR_BIT - 1 - index % CHAR_BIT;
            return ((mBits[index / CHAR_BIT] >> shift) & 1) == 1;
        }

        void set(std::ptrdiff_t index) {
            if (index < 0 || index >= mBitCount) {
                throw std::runtime_error("Out of bounds access in bit_vector.");
            }
            const char shift = CHAR_BIT - 1 - index % CHAR_BIT;
            const char mask = 1 << shift;
            mBits[index / CHAR_BIT] |= mask;
        }

        void clear(std::ptrdiff_t index) {
            if (index < 0 || index >= mBitCount) {
                throw std::runtime_error("Out of bounds access in bit_vector.");
            }
            const char shift = CHAR_BIT - 1 - index % CHAR_BIT;
            const char mask = ~(1 << shift);
            mBits[index / CHAR_BIT] &= mask;
        }

        // very inefficient if the compiler does not know how to optimize this
        std::ptrdiff_t countBitsSet() const {
            std::ptrdiff_t setBitCount = 0;
            for (std::ptrdiff_t i = 0; i < mBitCount; ++i) {
                if (get(i))
                    ++setBitCount;
            }
            return setBitCount;
        }

        gsl::span<const char> bitContainer() const {
            return { mBits.data(), mBits.size() };
        }

        void updateContainer(gsl::span<const char> newData) {
            if (newData.size() != mBits.size())
                throw std::runtime_error("bit_vector updateContainer input length mismatch.");
            std::copy(newData.begin(), newData.end(), mBits.begin());
        }

    private:
        std::vector<char> mBits;
        std::ptrdiff_t mBitCount;
    };

}
